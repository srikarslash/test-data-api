package com.srikar.controller;

import static spark.Spark.*;

import com.google.gson.Gson;
import com.srikar.entity.Blog;
import com.srikar.repository.UserService;
import com.srikar.repository.UserServiceImpl;
import com.srikar.service.BlogService;
import com.srikar.service.BlogServiceImpl;

public class Api {

	public static UserService userService = new UserServiceImpl();
	
	public static BlogService blogService = new BlogServiceImpl();

	public static void main(String[] args) {
		final Gson gson = new Gson();

		post("/add-post", (req, res) -> {
			Blog blog = gson.fromJson(req.body(), Blog.class);
			return blogService.addPost(blog);

		}, gson::toJson);
		
		get("/all-blogs",(req,res)->{
			res.type("application/json");
			return blogService.getAllBlogs();
		}, gson :: toJson);
		
		get("/delete",(req,res)->{
			res.type("application/json");
			return blogService.delete();
			
		}, gson :: toJson);
		
		get("/get-by-userName",(req,res)->{
			res.type("application/json");
			
			String user =  req.queryParams("user");

			System.out.println();
			return blogService.findByUserName(user);
		}
		, gson :: toJson);
		
		get("/get-by-test-env",(req,res)->{
			res.type("application/json");
			
			String test = req.queryParams("test");
			String env = req.queryParams("env");
			
			return blogService.getDataByUserEnv(test,env);
			
		},gson :: toJson);
	}

}
