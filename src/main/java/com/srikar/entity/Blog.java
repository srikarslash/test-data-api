package com.srikar.entity;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
public class Blog implements Serializable {
	
	private static final long serialVersionUID = -4506778592596073858L;
	@Id
	private ObjectId id;
	private String userName;
	private String title;
	private String description;
	private String auther;
	
	public Blog() {
		super();
	}
	
	//Get Id method
	//Get ID method in Develop branch
	
	public ObjectId getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuther() {
		return auther;
	}

	public void setAuther(String auther) {
		this.auther = auther;
	}
		
	

}
