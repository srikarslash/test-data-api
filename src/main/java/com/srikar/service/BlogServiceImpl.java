package com.srikar.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.srikar.entity.Blog;
import com.srikar.repository.UserService;
import com.srikar.repository.UserServiceImpl;

public class BlogServiceImpl implements BlogService {

	public static UserService userService = new UserServiceImpl();

	Map<String, List<Blog>> finalDetails = new HashMap<>();
	Map<String, List<Blog>> testCases = new HashMap<>();

	List<Blog> details = new ArrayList<>();
	List<Blog> testDetails = new ArrayList<>();
	List<Blog> envDetails = new ArrayList<>();

	public Map<String, List<Blog>> findByUserName(String user) {

		/*
		 * addDetails();
		 * 
		 * if (user.hashCode() != 0) {
		 * 
		 * List<Blog> db = userService.findByUserName(user); details.clear();
		 * details.add(db.get(0)); finalDetails.put("Test Case", details);
		 * 
		 * return finalDetails;
		 * 
		 * }
		 * 
		 * finalDetails.put("Default", details);
		 * 
		 * return finalDetails;
		 */

		return null;
	}

	@Override
	public String addPost(Blog blog) {

		return userService.addPost(blog);
	}

	@Override
	public String delete() {

		return userService.delete();
	}

	@Override
	public List<Blog> getAllBlogs() {

		return userService.getAllBlogs();
	}

	@Override
	public Blog getDataByUserEnv(String test, String env) {

		if (details.isEmpty()) {
			addDetails("Default", details);
		}

		if (test.hashCode() != 0) {

			if (env.hashCode() != 0) {
				if (envDetails.isEmpty()) {
					addDetails("Env Details", envDetails);
				}

				return returnMap("Env Test Details", testCases, envDetails);
			}

			if (testDetails.isEmpty()) {
				addDetails("Test Defaults", testDetails);
			}

			return returnMap("TestDefaultDetails", testCases, testDetails);
		}

		return returnMap("Default", finalDetails, details);
	}

	public Blog returnMap(String key, Map<String, List<Blog>> map, List<Blog> list) {

		map.clear();
		map.put(key, list);
		return map.get(key).get(0);
		

	}

	private void addDetails(String details, List<Blog> list) {

		final String value = details;

		Blog x = new Blog();
		x.setId(null);
		x.setUserName(value);
		x.setTitle(value);
		x.setDescription(value);
		x.setAuther("Srikar");
		list.add(x);
	}

}
