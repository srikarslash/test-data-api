package com.srikar.service;

import java.util.List;
import java.util.Map;

import com.srikar.entity.Blog;

public interface BlogService {
	
	public String addPost(Blog blog);
	public String delete();
	public List<Blog> getAllBlogs();
	public Map<String,List<Blog>> findByUserName(String user);
	public Blog getDataByUserEnv(String user, String env);

}
