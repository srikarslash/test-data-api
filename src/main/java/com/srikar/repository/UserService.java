package com.srikar.repository;

import java.util.List;

import com.srikar.entity.Blog;

public interface UserService {
	
	public String addPost(Blog blog);
	public String delete();
	public List<Blog> getAllBlogs();
	public List<Blog> findByUserName (String id);
	
}
