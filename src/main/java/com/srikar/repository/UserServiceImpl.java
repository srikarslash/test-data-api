package com.srikar.repository;


import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.srikar.entity.Blog;

public class UserServiceImpl implements UserService {

	MongoClient mongoClient = new MongoClient("localhost", 27017);
	Datastore datastore = new Morphia().createDatastore(mongoClient, "blog");
	

	public String addPost(Blog blog) {
		
		
		datastore.save(blog);
		return "Blog saved";
	}
	
	public String updatePost(Blog blog) {
		datastore.merge(blog);
		return "Data Updated";
	}
	
	public String delete() {
	
			MongoClient mongoClient = new MongoClient();
			DB db = mongoClient.getDB("blog");
			db.dropDatabase();
			mongoClient.close();
		    return "Delete and drop";
		
	}

	public List<Blog> getAllBlogs() {
		
		Query<Blog> query = datastore.find(Blog.class);
        final List<Blog> list = query.asList();
			return list;
		
	}
	
	public List<Blog> findByUserName (String id){
		
		return datastore.find(Blog.class).filter("userName =", id).asList();
	}
	

}
