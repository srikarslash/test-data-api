package com.srikar.test;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import io.restassured.http.ContentType;

public class TestBlog {
	
	@Parameters({"testName","envName"})
	@Test
	public void testGetUser(String param1, String param2) {

		
	    String responseBody = given().accept(ContentType.JSON)
		.param("test",param1)
		.param("env", param2)
		.when()
		.get("http://localhost:4567/get-by-test-env")
		.thenReturn()
		.asString();
				
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(responseBody);
		String prettyJsonString = gson.toJson(je);
		System.out.println(prettyJsonString);
		System.out.println("************************************************************");

	}
	
}
